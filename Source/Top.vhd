----------------------------------------------------------------------------------
-- Company: 
-- Engineer: LILYYYYYYYYYYYYYYYYYYYYYYYYYY
-- 
-- Create Date: 04/25/2023 04:35:06 PM
-- Design Name: 
-- Module Name: Top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity Top is
    Port (
    SOM_UART_BSL_TO_OBC : in STD_LOGIC;
    SOM_UART_OBC_TO_BSL : out STD_LOGIC;
    
    FRAM_SPI_MOSI : inout STD_LOGIC;
    FRAM_SPI_MISO : inout STD_LOGIC;
    FRAM_SPI_CLK : inout STD_LOGIC;
    FRAM0_SPI_CS_B : inout STD_LOGIC;
    FRAM1_SPI_CS_B : inout STD_LOGIC;
    FRAM2_SPI_CS_B : inout STD_LOGIC;
    
    FRAM1_SPI_MOSI : inout STD_LOGIC;
    FRAM1_SPI_MISO : inout STD_LOGIC;
    FRAM1_SPI_CLK : inout STD_LOGIC;
    FRAM1_SPI_CS_B1 : inout STD_LOGIC;
    
    GPIO0 : in STD_LOGIC;
    GPIO1 :  inout STD_LOGIC_VECTOR ( 6 downto 0 ); 
    GPIO2 :  inout STD_LOGIC;
    GPIO3 :  out  STD_LOGIC_VECTOR ( 0 downto 0 ); 
    GPIO4 :  inout  STD_LOGIC_VECTOR ( 0 downto 0 ); 
    GPIO5 :  out STD_LOGIC;
    GPIO6 :  out STD_LOGIC;
    
    NOR0_QSPI_DQ0 : inout STD_LOGIC;
    NOR0_QSPI_DQ1 : inout STD_LOGIC;
    NOR0_QSPI_DQ2 :inout STD_LOGIC;
    NOR0_QSPI_DQ3 :inout STD_LOGIC;
    NOR0_QSPI_CLK :inout STD_LOGIC;
    NOR0_QSPI_CS_B :inout STD_LOGIC_VECTOR ( 0 to 0 );
    NOR0_RESET_B : out STD_LOGIC_VECTOR ( 0 to 0 );
    
    NOR1_QSPI_DQ0 :inout STD_LOGIC;
    NOR1_QSPI_DQ1 :inout STD_LOGIC;
    NOR1_QSPI_DQ2 :inout STD_LOGIC;
    NOR1_QSPI_DQ3 :inout STD_LOGIC;
    NOR1_QSPI_CLK :inout STD_LOGIC;
    NOR1_QSPI_CS_B :inout STD_LOGIC_VECTOR ( 0 to 0 );
    NOR1_RESET_B :out STD_LOGIC_VECTOR ( 0 to 0 );
    
    NOR2_QSPI_DQ0 :inout STD_LOGIC;
    NOR2_QSPI_DQ1 :inout STD_LOGIC;
    NOR2_QSPI_DQ2 :inout STD_LOGIC;
    NOR2_QSPI_DQ3 :inout STD_LOGIC;
    NOR2_QSPI_CLK :inout STD_LOGIC;
    NOR2_QSPI_CS_B :inout STD_LOGIC_VECTOR ( 0 to 0 );
    NOR2_RESET_B :out STD_LOGIC_VECTOR ( 0 to 0 );
    
    TELEM_CS : inout STD_LOGIC;
    TELEM_SD0 : inout STD_LOGIC;
    TELEM_SCLK : inout STD_LOGIC;
    
    --UART pins 
    D0_rx :  inout STD_LOGIC;
    D0_tx :  out STD_LOGIC;
    D1_rx :  inout STD_LOGIC;
    D1_tx :  out STD_LOGIC;
    D2_rx :  inout STD_LOGIC;
    D2_tx :  out STD_LOGIC;
    D3_rx :  inout STD_LOGIC;
    D3_tx :  out STD_LOGIC;
    D4_rx :  inout STD_LOGIC;
    D4_tx :  out STD_LOGIC;
    D5_rx :  inout STD_LOGIC;
    D5_tx :  out STD_LOGIC;
    D6_rx :  inout STD_LOGIC;
    D6_tx :  out STD_LOGIC
    
   );
    
end Top;

architecture Behavioral of Top is

component QuaSAR_wrapper is
    port (
        BSL_rxd : in STD_LOGIC;
        BSL_txd : out STD_LOGIC;
        FLOW_CTRL_tri_i : in STD_LOGIC_VECTOR ( 0 to 0 );
        FRAM_io0_io : inout STD_LOGIC;
        FRAM_io1_io : inout STD_LOGIC;
        FRAM_sck_io : inout STD_LOGIC; --route each bit to cs
         
          
        GNCSlice_rxd : in STD_LOGIC;
        GNCSlice_txd : out STD_LOGIC;
        IO_tri_io : inout STD_LOGIC;
        LED_tri_o : out STD_LOGIC_VECTOR ( 0 to 0 );
        
       
        NOR1_io0_io : inout STD_LOGIC;
        NOR1_io1_io : inout STD_LOGIC;
        NOR1_io2_io : inout std_logic;
        NOR1_io3_io : inout STD_LOGIC;
        NOR1_sck_io : inout STD_LOGIC;
        NOR1_ss_io : inout STD_LOGIC_VECTOR ( 0 to 0 );
        
        NOR2_io0_io : inout STD_LOGIC;
        NOR2_io1_io : inout STD_LOGIC;
        NOR2_sck_io : inout STD_LOGIC;
        NOR2_ss_io : inout STD_LOGIC_VECTOR ( 0 to 0 );
        
        NOR3_io0_io : inout STD_LOGIC;
        NOR3_io1_io : inout STD_LOGIC;
        NOR3_io2_io : inout std_logic;
        NOR3_io3_io : inout STD_LOGIC;
        NOR3_sck_io : inout STD_LOGIC;
        NOR3_ss_io : inout STD_LOGIC_VECTOR ( 0 to 0 );
        
        PayloadSlice_scl_io : inout STD_LOGIC;
        PayloadSlice_sda_io : inout STD_LOGIC;
        
        --unrouted
        ProgrammableLogic_rxd : in STD_LOGIC;
        ProgrammableLogic_txd : out STD_LOGIC;
        
        RadioSlice_rxd : in STD_LOGIC;
        RadioSlice_txd : out STD_LOGIC;
        
        RemotePort_rxd : in STD_LOGIC;
        RemotePort_txd : out STD_LOGIC;
        
        SecondaryOBC_EthernetSwitch_rxd : in STD_LOGIC;
        SecondaryOBC_EthernetSwitch_txd : out STD_LOGIC;
        
        Supervisor_rxd : in STD_LOGIC;
        Supervisor_txd : out STD_LOGIC;
        
        Supervisor_FRAM_io0_io : inout STD_LOGIC;
        Supervisor_FRAM_io1_io : inout STD_LOGIC;
        Supervisor_FRAM_sck_io : inout STD_LOGIC;
        Supervisor_FRAM_ss_io : inout STD_LOGIC;
        
        WP_DIS_tri_o : out STD_LOGIC_VECTOR ( 0 downto 0 );
        uart_rtl_rxd : in STD_LOGIC;
        uart_rtl_txd : out STD_LOGIC;
        SYS_RST_tri_o : out STD_LOGIC_VECTOR ( 0 downto 0 )
    );
    
end component QuaSAR_wrapper;
  signal FRAM_io0_i : STD_LOGIC;
  signal FRAM_io0_o : STD_LOGIC;
  signal FRAM_io0_t : STD_LOGIC;
  signal FRAM_io1_i : STD_LOGIC;
  signal FRAM_io1_o : STD_LOGIC;
  signal FRAM_io1_t : STD_LOGIC;
  signal FRAM_sck_i : STD_LOGIC;
  signal FRAM_sck_o : STD_LOGIC;
  signal FRAM_sck_t : STD_LOGIC;
  signal FRAM_ss_i_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal FRAM_ss_i_1 : STD_LOGIC_VECTOR ( 1 to 1 );
  signal FRAM_ss_i_2 : STD_LOGIC_VECTOR ( 2 to 2 );
  signal FRAM_ss_io_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal FRAM_ss_io_2 : STD_LOGIC_VECTOR ( 2 to 2 );
  signal FRAM_ss_o_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal FRAM_ss_o_1 : STD_LOGIC_VECTOR ( 1 to 1 );
  signal FRAM_ss_o_2 : STD_LOGIC_VECTOR ( 2 to 2 );
  signal FRAM_ss_t : STD_LOGIC;
  signal IO_tri_i_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal IO_tri_io_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal IO_tri_o_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal IO_tri_t_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NOR1_io0_i : STD_LOGIC;
  signal NOR1_io0_o : STD_LOGIC;
  signal NOR1_io0_t : STD_LOGIC;
  signal NOR1_io1_i : STD_LOGIC;
  signal NOR1_io1_o : STD_LOGIC;
  signal NOR1_io1_t : STD_LOGIC;
  signal NOR1_io2_i : STD_LOGIC;
  signal NOR1_io2_o : STD_LOGIC;
  signal NOR1_io2_t : STD_LOGIC;
  signal NOR1_io3_i : STD_LOGIC;
  signal NOR1_io3_o : STD_LOGIC;
  signal NOR1_io3_t : STD_LOGIC;
  signal NOR1_sck_i : STD_LOGIC;
  signal NOR1_sck_o : STD_LOGIC;
  signal NOR1_sck_t : STD_LOGIC;
  signal NOR1_ss_i_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NOR1_ss_io_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NOR1_ss_o_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NOR1_ss_t : STD_LOGIC;
  signal NOR2_io0_i : STD_LOGIC;
  signal NOR2_io0_o : STD_LOGIC;
  signal NOR2_io0_t : STD_LOGIC;
  signal NOR2_io1_i : STD_LOGIC;
  signal NOR2_io1_o : STD_LOGIC;
  signal NOR2_io1_t : STD_LOGIC;
  signal NOR2_sck_i : STD_LOGIC;
  signal NOR2_sck_o : STD_LOGIC;
  signal NOR2_sck_t : STD_LOGIC;
  signal NOR2_ss_i_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NOR2_ss_io_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NOR2_ss_o_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NOR2_ss_t : STD_LOGIC;
  signal NOR3_io0_i : STD_LOGIC;
  signal NOR3_io0_o : STD_LOGIC;
  signal NOR3_io0_t : STD_LOGIC;
  signal NOR3_io1_i : STD_LOGIC;
  signal NOR3_io1_o : STD_LOGIC;
  signal NOR3_io1_t : STD_LOGIC;
  signal NOR3_io2_i : STD_LOGIC;
  signal NOR3_io2_o : STD_LOGIC;
  signal NOR3_io2_t : STD_LOGIC;
  signal NOR3_io3_i : STD_LOGIC;
  signal NOR3_io3_o : STD_LOGIC;
  signal NOR3_io3_t : STD_LOGIC;
  signal NOR3_sck_i : STD_LOGIC;
  signal NOR3_sck_o : STD_LOGIC;
  signal NOR3_sck_t : STD_LOGIC;
  signal NOR3_ss_i_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NOR3_ss_io_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NOR3_ss_o_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NOR3_ss_t : STD_LOGIC;
  signal PayloadSlice_scl_i : STD_LOGIC;
  signal PayloadSlice_scl_o : STD_LOGIC;
  signal PayloadSlice_scl_t : STD_LOGIC;
  signal PayloadSlice_sda_i : STD_LOGIC;
  signal PayloadSlice_sda_o : STD_LOGIC;
  signal PayloadSlice_sda_t : STD_LOGIC;
  signal Supervisor_FRAM_io0_i : STD_LOGIC;
  signal Supervisor_FRAM_io0_o : STD_LOGIC;
  signal Supervisor_FRAM_io0_t : STD_LOGIC;
  signal Supervisor_FRAM_io1_i : STD_LOGIC;
  signal Supervisor_FRAM_io1_o : STD_LOGIC;
  signal Supervisor_FRAM_io1_t : STD_LOGIC;
  signal Supervisor_FRAM_sck_i : STD_LOGIC;
  signal Supervisor_FRAM_sck_o : STD_LOGIC;
  signal Supervisor_FRAM_sck_t : STD_LOGIC;
  signal Supervisor_FRAM_ss_i_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal Supervisor_FRAM_ss_io_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal Supervisor_FRAM_ss_o_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal Supervisor_FRAM_ss_t : STD_LOGIC;
  
begin
QuaSAR_i: component QuaSAR_wrapper
     port map (
      
      FRAM_io0_io  => FRAM_SPI_MISO,
      FRAM_io1_io  => FRAM_SPI_MOSI,
      FRAM_sck_io => FRAM_SPI_CLK,
      
      Supervisor_FRAM_io0_io => FRAM1_SPI_MISO,
      Supervisor_FRAM_io1_io => FRAM1_SPI_MOSI,
      Supervisor_FRAM_sck_io => FRAM1_SPI_CLK,
      Supervisor_FRAM_ss_io  => FRAM1_SPI_CS_B1,
      
      
      NOR1_io0_io => NOR0_QSPI_DQ0,
      NOR1_io1_io => NOR0_QSPI_DQ1, 
      NOR1_io2_io => NOR0_QSPI_DQ2,
      NOR1_io3_io => NOR0_QSPI_DQ3,
      NOR1_sck_io => NOR0_QSPI_CLK,
      NOR1_ss_io => NOR0_QSPI_CS_B,
      SYS_RST_tri_o => NOR0_RESET_B,
      
      NOR2_io0_io => NOR1_QSPI_DQ0,
      NOR2_io1_io => NOR1_QSPI_DQ1,
      NOR2_sck_io => NOR1_QSPI_CLK,
      NOR2_ss_io => NOR1_QSPI_CS_B,
      --SYS_RST_tri_o => NOR1_RESET_B,
      
      NOR3_io0_io => NOR2_QSPI_DQ0,
      NOR3_io1_io => NOR2_QSPI_DQ1, 
      NOR3_io2_io => NOR2_QSPI_DQ2,
      NOR3_io3_io => NOR2_QSPI_DQ3,
      NOR3_sck_io => NOR2_QSPI_CLK,
      NOR3_ss_io => NOR2_QSPI_CS_B,
      --SYS_RST_tri_o (0)=> NOR2_RESET_B,
      
      BSL_rxd => SOM_UART_BSL_TO_OBC,
      BSL_txd => SOM_UART_OBC_TO_BSL ,
    
      PayloadSlice_scl_io => TELEM_SCLK,
      PayloadSlice_sda_io => TELEM_SD0,
      
      FLOW_CTRL_tri_i(0) => GPIO0,
      IO_tri_io => GPIO2,
      LED_tri_o => GPIO3,    
      WP_DIS_tri_o(0) => GPIO5,
      
      --UART Routings
      GNCSlice_rxd => D0_rx,
      GNCSlice_txd => D0_tx,
      ProgrammableLogic_rxd => D1_rx,
      ProgrammableLogic_txd => D1_tx,
      RadioSlice_rxd => D2_rx,
      RadioSlice_txd => D2_tx,
      RemotePort_rxd => D3_rx,
      RemotePort_txd => D3_tx,
      SecondaryOBC_EthernetSwitch_rxd => D4_rx,
      SecondaryOBC_EthernetSwitch_txd => D4_tx,
      Supervisor_rxd => D5_rx,
      Supervisor_txd => D5_tx,
      
      uart_rtl_rxd => D6_rx,
      uart_rtl_txd => D6_tx


    );


  



end Behavioral;

