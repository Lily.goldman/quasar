--Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2022.1 (win64) Build 3526262 Mon Apr 18 15:48:16 MDT 2022
--Date        : Wed May  3 22:14:33 2023
--Host        : PC21 running 64-bit major release  (build 9200)
--Command     : generate_target QuaSAR_wrapper.bd
--Design      : QuaSAR_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity QuaSAR_wrapper is
  port (
    BSL_rxd : in STD_LOGIC;
    BSL_txd : out STD_LOGIC;
    FLOW_CTRL_tri_i : in STD_LOGIC_VECTOR ( 0 to 0 );
    FRAM_io0_io : inout STD_LOGIC;
    FRAM_io1_io : inout STD_LOGIC;
    FRAM_sck_io : inout STD_LOGIC;
    FRAM_ss_io : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    GNCSlice_PPS_tri_i : in STD_LOGIC_VECTOR ( 0 to 0 );
    GNCSlice_rxd : in STD_LOGIC;
    GNCSlice_txd : out STD_LOGIC;
    IO_tri_io : inout STD_LOGIC_VECTOR ( 0 to 0 );
    LED_tri_o : out STD_LOGIC_VECTOR ( 6 downto 0 );
    NOR1_io0_io : inout STD_LOGIC;
    NOR1_io1_io : inout STD_LOGIC;
    NOR1_io2_io : inout STD_LOGIC;
    NOR1_io3_io : inout STD_LOGIC;
    NOR1_sck_io : inout STD_LOGIC;
    NOR1_ss_io : inout STD_LOGIC_VECTOR ( 0 to 0 );
    NOR2_io0_io : inout STD_LOGIC;
    NOR2_io1_io : inout STD_LOGIC;
    NOR2_sck_io : inout STD_LOGIC;
    NOR2_ss_io : inout STD_LOGIC_VECTOR ( 0 to 0 );
    NOR3_io0_io : inout STD_LOGIC;
    NOR3_io1_io : inout STD_LOGIC;
    NOR3_io2_io : inout STD_LOGIC;
    NOR3_io3_io : inout STD_LOGIC;
    NOR3_sck_io : inout STD_LOGIC;
    NOR3_ss_io : inout STD_LOGIC_VECTOR ( 0 to 0 );
    PayloadSlice_PPS_tri_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    PayloadSlice_scl_io : inout STD_LOGIC;
    PayloadSlice_sda_io : inout STD_LOGIC;
    ProgrammableLogic_rxd : in STD_LOGIC;
    ProgrammableLogic_txd : out STD_LOGIC;
    RadioSlice_rxd : in STD_LOGIC;
    RadioSlice_txd : out STD_LOGIC;
    RemotePort_rxd : in STD_LOGIC;
    RemotePort_txd : out STD_LOGIC;
    SYS_RST_tri_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    SecondaryOBC_EthernetSwitch_rxd : in STD_LOGIC;
    SecondaryOBC_EthernetSwitch_txd : out STD_LOGIC;
    Supervisor_FRAM_io0_io : inout STD_LOGIC;
    Supervisor_FRAM_io1_io : inout STD_LOGIC;
    Supervisor_FRAM_sck_io : inout STD_LOGIC;
    Supervisor_FRAM_ss_io : inout STD_LOGIC_VECTOR ( 0 to 0 );
    Supervisor_rxd : in STD_LOGIC;
    Supervisor_txd : out STD_LOGIC;
    WP_DIS_tri_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    uart_rtl_rxd : in STD_LOGIC;
    uart_rtl_txd : out STD_LOGIC
  );
end QuaSAR_wrapper;

architecture STRUCTURE of QuaSAR_wrapper is
  component QuaSAR is
  port (
    uart_rtl_rxd : in STD_LOGIC;
    uart_rtl_txd : out STD_LOGIC;
    GNCSlice_PPS_tri_i : in STD_LOGIC_VECTOR ( 0 to 0 );
    PayloadSlice_PPS_tri_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    IO_tri_i : in STD_LOGIC_VECTOR ( 0 to 0 );
    IO_tri_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    IO_tri_t : out STD_LOGIC_VECTOR ( 0 to 0 );
    LED_tri_o : out STD_LOGIC_VECTOR ( 6 downto 0 );
    SYS_RST_tri_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    WP_DIS_tri_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    FLOW_CTRL_tri_i : in STD_LOGIC_VECTOR ( 0 to 0 );
    PayloadSlice_scl_i : in STD_LOGIC;
    PayloadSlice_scl_o : out STD_LOGIC;
    PayloadSlice_scl_t : out STD_LOGIC;
    PayloadSlice_sda_i : in STD_LOGIC;
    PayloadSlice_sda_o : out STD_LOGIC;
    PayloadSlice_sda_t : out STD_LOGIC;
    FRAM_io0_i : in STD_LOGIC;
    FRAM_io0_o : out STD_LOGIC;
    FRAM_io0_t : out STD_LOGIC;
    FRAM_io1_i : in STD_LOGIC;
    FRAM_io1_o : out STD_LOGIC;
    FRAM_io1_t : out STD_LOGIC;
    FRAM_sck_i : in STD_LOGIC;
    FRAM_sck_o : out STD_LOGIC;
    FRAM_sck_t : out STD_LOGIC;
    FRAM_ss_i : in STD_LOGIC_VECTOR ( 2 downto 0 );
    FRAM_ss_o : out STD_LOGIC_VECTOR ( 2 downto 0 );
    FRAM_ss_t : out STD_LOGIC;
    NOR1_io0_i : in STD_LOGIC;
    NOR1_io0_o : out STD_LOGIC;
    NOR1_io0_t : out STD_LOGIC;
    NOR1_io1_i : in STD_LOGIC;
    NOR1_io1_o : out STD_LOGIC;
    NOR1_io1_t : out STD_LOGIC;
    NOR1_io2_i : in STD_LOGIC;
    NOR1_io2_o : out STD_LOGIC;
    NOR1_io2_t : out STD_LOGIC;
    NOR1_io3_i : in STD_LOGIC;
    NOR1_io3_o : out STD_LOGIC;
    NOR1_io3_t : out STD_LOGIC;
    NOR1_sck_i : in STD_LOGIC;
    NOR1_sck_o : out STD_LOGIC;
    NOR1_sck_t : out STD_LOGIC;
    NOR1_ss_i : in STD_LOGIC_VECTOR ( 0 to 0 );
    NOR1_ss_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    NOR1_ss_t : out STD_LOGIC;
    NOR2_io0_i : in STD_LOGIC;
    NOR2_io0_o : out STD_LOGIC;
    NOR2_io0_t : out STD_LOGIC;
    NOR2_io1_i : in STD_LOGIC;
    NOR2_io1_o : out STD_LOGIC;
    NOR2_io1_t : out STD_LOGIC;
    NOR2_sck_i : in STD_LOGIC;
    NOR2_sck_o : out STD_LOGIC;
    NOR2_sck_t : out STD_LOGIC;
    NOR2_ss_i : in STD_LOGIC_VECTOR ( 0 to 0 );
    NOR2_ss_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    NOR2_ss_t : out STD_LOGIC;
    NOR3_io0_i : in STD_LOGIC;
    NOR3_io0_o : out STD_LOGIC;
    NOR3_io0_t : out STD_LOGIC;
    NOR3_io1_i : in STD_LOGIC;
    NOR3_io1_o : out STD_LOGIC;
    NOR3_io1_t : out STD_LOGIC;
    NOR3_io2_i : in STD_LOGIC;
    NOR3_io2_o : out STD_LOGIC;
    NOR3_io2_t : out STD_LOGIC;
    NOR3_io3_i : in STD_LOGIC;
    NOR3_io3_o : out STD_LOGIC;
    NOR3_io3_t : out STD_LOGIC;
    NOR3_sck_i : in STD_LOGIC;
    NOR3_sck_o : out STD_LOGIC;
    NOR3_sck_t : out STD_LOGIC;
    NOR3_ss_i : in STD_LOGIC_VECTOR ( 0 to 0 );
    NOR3_ss_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    NOR3_ss_t : out STD_LOGIC;
    Supervisor_FRAM_io0_i : in STD_LOGIC;
    Supervisor_FRAM_io0_o : out STD_LOGIC;
    Supervisor_FRAM_io0_t : out STD_LOGIC;
    Supervisor_FRAM_io1_i : in STD_LOGIC;
    Supervisor_FRAM_io1_o : out STD_LOGIC;
    Supervisor_FRAM_io1_t : out STD_LOGIC;
    Supervisor_FRAM_sck_i : in STD_LOGIC;
    Supervisor_FRAM_sck_o : out STD_LOGIC;
    Supervisor_FRAM_sck_t : out STD_LOGIC;
    Supervisor_FRAM_ss_i : in STD_LOGIC_VECTOR ( 0 to 0 );
    Supervisor_FRAM_ss_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    Supervisor_FRAM_ss_t : out STD_LOGIC;
    Supervisor_rxd : in STD_LOGIC;
    Supervisor_txd : out STD_LOGIC;
    BSL_rxd : in STD_LOGIC;
    BSL_txd : out STD_LOGIC;
    SecondaryOBC_EthernetSwitch_rxd : in STD_LOGIC;
    SecondaryOBC_EthernetSwitch_txd : out STD_LOGIC;
    GNCSlice_rxd : in STD_LOGIC;
    GNCSlice_txd : out STD_LOGIC;
    RadioSlice_rxd : in STD_LOGIC;
    RadioSlice_txd : out STD_LOGIC;
    ProgrammableLogic_rxd : in STD_LOGIC;
    ProgrammableLogic_txd : out STD_LOGIC;
    RemotePort_rxd : in STD_LOGIC;
    RemotePort_txd : out STD_LOGIC
  );
  end component QuaSAR;
  component IOBUF is
  port (
    I : in STD_LOGIC;
    O : out STD_LOGIC;
    T : in STD_LOGIC;
    IO : inout STD_LOGIC
  );
  end component IOBUF;
  signal FRAM_io0_i : STD_LOGIC;
  signal FRAM_io0_o : STD_LOGIC;
  signal FRAM_io0_t : STD_LOGIC;
  signal FRAM_io1_i : STD_LOGIC;
  signal FRAM_io1_o : STD_LOGIC;
  signal FRAM_io1_t : STD_LOGIC;
  signal FRAM_sck_i : STD_LOGIC;
  signal FRAM_sck_o : STD_LOGIC;
  signal FRAM_sck_t : STD_LOGIC;
  signal FRAM_ss_i_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal FRAM_ss_i_1 : STD_LOGIC_VECTOR ( 1 to 1 );
  signal FRAM_ss_i_2 : STD_LOGIC_VECTOR ( 2 to 2 );
  signal FRAM_ss_io_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal FRAM_ss_io_1 : STD_LOGIC_VECTOR ( 1 to 1 );
  signal FRAM_ss_io_2 : STD_LOGIC_VECTOR ( 2 to 2 );
  signal FRAM_ss_o_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal FRAM_ss_o_1 : STD_LOGIC_VECTOR ( 1 to 1 );
  signal FRAM_ss_o_2 : STD_LOGIC_VECTOR ( 2 to 2 );
  signal FRAM_ss_t : STD_LOGIC;
  signal IO_tri_i_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal IO_tri_io_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal IO_tri_o_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal IO_tri_t_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NOR1_io0_i : STD_LOGIC;
  signal NOR1_io0_o : STD_LOGIC;
  signal NOR1_io0_t : STD_LOGIC;
  signal NOR1_io1_i : STD_LOGIC;
  signal NOR1_io1_o : STD_LOGIC;
  signal NOR1_io1_t : STD_LOGIC;
  signal NOR1_io2_i : STD_LOGIC;
  signal NOR1_io2_o : STD_LOGIC;
  signal NOR1_io2_t : STD_LOGIC;
  signal NOR1_io3_i : STD_LOGIC;
  signal NOR1_io3_o : STD_LOGIC;
  signal NOR1_io3_t : STD_LOGIC;
  signal NOR1_sck_i : STD_LOGIC;
  signal NOR1_sck_o : STD_LOGIC;
  signal NOR1_sck_t : STD_LOGIC;
  signal NOR1_ss_i_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NOR1_ss_io_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NOR1_ss_o_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NOR1_ss_t : STD_LOGIC;
  signal NOR2_io0_i : STD_LOGIC;
  signal NOR2_io0_o : STD_LOGIC;
  signal NOR2_io0_t : STD_LOGIC;
  signal NOR2_io1_i : STD_LOGIC;
  signal NOR2_io1_o : STD_LOGIC;
  signal NOR2_io1_t : STD_LOGIC;
  signal NOR2_sck_i : STD_LOGIC;
  signal NOR2_sck_o : STD_LOGIC;
  signal NOR2_sck_t : STD_LOGIC;
  signal NOR2_ss_i_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NOR2_ss_io_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NOR2_ss_o_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NOR2_ss_t : STD_LOGIC;
  signal NOR3_io0_i : STD_LOGIC;
  signal NOR3_io0_o : STD_LOGIC;
  signal NOR3_io0_t : STD_LOGIC;
  signal NOR3_io1_i : STD_LOGIC;
  signal NOR3_io1_o : STD_LOGIC;
  signal NOR3_io1_t : STD_LOGIC;
  signal NOR3_io2_i : STD_LOGIC;
  signal NOR3_io2_o : STD_LOGIC;
  signal NOR3_io2_t : STD_LOGIC;
  signal NOR3_io3_i : STD_LOGIC;
  signal NOR3_io3_o : STD_LOGIC;
  signal NOR3_io3_t : STD_LOGIC;
  signal NOR3_sck_i : STD_LOGIC;
  signal NOR3_sck_o : STD_LOGIC;
  signal NOR3_sck_t : STD_LOGIC;
  signal NOR3_ss_i_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NOR3_ss_io_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NOR3_ss_o_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NOR3_ss_t : STD_LOGIC;
  signal PayloadSlice_scl_i : STD_LOGIC;
  signal PayloadSlice_scl_o : STD_LOGIC;
  signal PayloadSlice_scl_t : STD_LOGIC;
  signal PayloadSlice_sda_i : STD_LOGIC;
  signal PayloadSlice_sda_o : STD_LOGIC;
  signal PayloadSlice_sda_t : STD_LOGIC;
  signal Supervisor_FRAM_io0_i : STD_LOGIC;
  signal Supervisor_FRAM_io0_o : STD_LOGIC;
  signal Supervisor_FRAM_io0_t : STD_LOGIC;
  signal Supervisor_FRAM_io1_i : STD_LOGIC;
  signal Supervisor_FRAM_io1_o : STD_LOGIC;
  signal Supervisor_FRAM_io1_t : STD_LOGIC;
  signal Supervisor_FRAM_sck_i : STD_LOGIC;
  signal Supervisor_FRAM_sck_o : STD_LOGIC;
  signal Supervisor_FRAM_sck_t : STD_LOGIC;
  signal Supervisor_FRAM_ss_i_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal Supervisor_FRAM_ss_io_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal Supervisor_FRAM_ss_o_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal Supervisor_FRAM_ss_t : STD_LOGIC;
begin
FRAM_io0_iobuf: component IOBUF
     port map (
      I => FRAM_io0_o,
      IO => FRAM_io0_io,
      O => FRAM_io0_i,
      T => FRAM_io0_t
    );
FRAM_io1_iobuf: component IOBUF
     port map (
      I => FRAM_io1_o,
      IO => FRAM_io1_io,
      O => FRAM_io1_i,
      T => FRAM_io1_t
    );
FRAM_sck_iobuf: component IOBUF
     port map (
      I => FRAM_sck_o,
      IO => FRAM_sck_io,
      O => FRAM_sck_i,
      T => FRAM_sck_t
    );
FRAM_ss_iobuf_0: component IOBUF
     port map (
      I => FRAM_ss_o_0(0),
      IO => FRAM_ss_io(0),
      O => FRAM_ss_i_0(0),
      T => FRAM_ss_t
    );
FRAM_ss_iobuf_1: component IOBUF
     port map (
      I => FRAM_ss_o_1(1),
      IO => FRAM_ss_io(1),
      O => FRAM_ss_i_1(1),
      T => FRAM_ss_t
    );
FRAM_ss_iobuf_2: component IOBUF
     port map (
      I => FRAM_ss_o_2(2),
      IO => FRAM_ss_io(2),
      O => FRAM_ss_i_2(2),
      T => FRAM_ss_t
    );
IO_tri_iobuf_0: component IOBUF
     port map (
      I => IO_tri_o_0(0),
      IO => IO_tri_io(0),
      O => IO_tri_i_0(0),
      T => IO_tri_t_0(0)
    );
NOR1_io0_iobuf: component IOBUF
     port map (
      I => NOR1_io0_o,
      IO => NOR1_io0_io,
      O => NOR1_io0_i,
      T => NOR1_io0_t
    );
NOR1_io1_iobuf: component IOBUF
     port map (
      I => NOR1_io1_o,
      IO => NOR1_io1_io,
      O => NOR1_io1_i,
      T => NOR1_io1_t
    );
NOR1_io2_iobuf: component IOBUF
     port map (
      I => NOR1_io2_o,
      IO => NOR1_io2_io,
      O => NOR1_io2_i,
      T => NOR1_io2_t
    );
NOR1_io3_iobuf: component IOBUF
     port map (
      I => NOR1_io3_o,
      IO => NOR1_io3_io,
      O => NOR1_io3_i,
      T => NOR1_io3_t
    );
NOR1_sck_iobuf: component IOBUF
     port map (
      I => NOR1_sck_o,
      IO => NOR1_sck_io,
      O => NOR1_sck_i,
      T => NOR1_sck_t
    );
NOR1_ss_iobuf_0: component IOBUF
     port map (
      I => NOR1_ss_o_0(0),
      IO => NOR1_ss_io(0),
      O => NOR1_ss_i_0(0),
      T => NOR1_ss_t
    );
NOR2_io0_iobuf: component IOBUF
     port map (
      I => NOR2_io0_o,
      IO => NOR2_io0_io,
      O => NOR2_io0_i,
      T => NOR2_io0_t
    );
NOR2_io1_iobuf: component IOBUF
     port map (
      I => NOR2_io1_o,
      IO => NOR2_io1_io,
      O => NOR2_io1_i,
      T => NOR2_io1_t
    );
NOR2_sck_iobuf: component IOBUF
     port map (
      I => NOR2_sck_o,
      IO => NOR2_sck_io,
      O => NOR2_sck_i,
      T => NOR2_sck_t
    );
NOR2_ss_iobuf_0: component IOBUF
     port map (
      I => NOR2_ss_o_0(0),
      IO => NOR2_ss_io(0),
      O => NOR2_ss_i_0(0),
      T => NOR2_ss_t
    );
NOR3_io0_iobuf: component IOBUF
     port map (
      I => NOR3_io0_o,
      IO => NOR3_io0_io,
      O => NOR3_io0_i,
      T => NOR3_io0_t
    );
NOR3_io1_iobuf: component IOBUF
     port map (
      I => NOR3_io1_o,
      IO => NOR3_io1_io,
      O => NOR3_io1_i,
      T => NOR3_io1_t
    );
NOR3_io2_iobuf: component IOBUF
     port map (
      I => NOR3_io2_o,
      IO => NOR3_io2_io,
      O => NOR3_io2_i,
      T => NOR3_io2_t
    );
NOR3_io3_iobuf: component IOBUF
     port map (
      I => NOR3_io3_o,
      IO => NOR3_io3_io,
      O => NOR3_io3_i,
      T => NOR3_io3_t
    );
NOR3_sck_iobuf: component IOBUF
     port map (
      I => NOR3_sck_o,
      IO => NOR3_sck_io,
      O => NOR3_sck_i,
      T => NOR3_sck_t
    );
NOR3_ss_iobuf_0: component IOBUF
     port map (
      I => NOR3_ss_o_0(0),
      IO => NOR3_ss_io(0),
      O => NOR3_ss_i_0(0),
      T => NOR3_ss_t
    );
PayloadSlice_scl_iobuf: component IOBUF
     port map (
      I => PayloadSlice_scl_o,
      IO => PayloadSlice_scl_io,
      O => PayloadSlice_scl_i,
      T => PayloadSlice_scl_t
    );
PayloadSlice_sda_iobuf: component IOBUF
     port map (
      I => PayloadSlice_sda_o,
      IO => PayloadSlice_sda_io,
      O => PayloadSlice_sda_i,
      T => PayloadSlice_sda_t
    );
QuaSAR_i: component QuaSAR
     port map (
      BSL_rxd => BSL_rxd,
      BSL_txd => BSL_txd,
      FLOW_CTRL_tri_i(0) => FLOW_CTRL_tri_i(0),
      FRAM_io0_i => FRAM_io0_i,
      FRAM_io0_o => FRAM_io0_o,
      FRAM_io0_t => FRAM_io0_t,
      FRAM_io1_i => FRAM_io1_i,
      FRAM_io1_o => FRAM_io1_o,
      FRAM_io1_t => FRAM_io1_t,
      FRAM_sck_i => FRAM_sck_i,
      FRAM_sck_o => FRAM_sck_o,
      FRAM_sck_t => FRAM_sck_t,
      FRAM_ss_i(2) => FRAM_ss_i_2(2),
      FRAM_ss_i(1) => FRAM_ss_i_1(1),
      FRAM_ss_i(0) => FRAM_ss_i_0(0),
      FRAM_ss_o(2) => FRAM_ss_o_2(2),
      FRAM_ss_o(1) => FRAM_ss_o_1(1),
      FRAM_ss_o(0) => FRAM_ss_o_0(0),
      FRAM_ss_t => FRAM_ss_t,
      GNCSlice_PPS_tri_i(0) => GNCSlice_PPS_tri_i(0),
      GNCSlice_rxd => GNCSlice_rxd,
      GNCSlice_txd => GNCSlice_txd,
      IO_tri_i(0) => IO_tri_i_0(0),
      IO_tri_o(0) => IO_tri_o_0(0),
      IO_tri_t(0) => IO_tri_t_0(0),
      LED_tri_o(6 downto 0) => LED_tri_o(6 downto 0),
      NOR1_io0_i => NOR1_io0_i,
      NOR1_io0_o => NOR1_io0_o,
      NOR1_io0_t => NOR1_io0_t,
      NOR1_io1_i => NOR1_io1_i,
      NOR1_io1_o => NOR1_io1_o,
      NOR1_io1_t => NOR1_io1_t,
      NOR1_io2_i => NOR1_io2_i,
      NOR1_io2_o => NOR1_io2_o,
      NOR1_io2_t => NOR1_io2_t,
      NOR1_io3_i => NOR1_io3_i,
      NOR1_io3_o => NOR1_io3_o,
      NOR1_io3_t => NOR1_io3_t,
      NOR1_sck_i => NOR1_sck_i,
      NOR1_sck_o => NOR1_sck_o,
      NOR1_sck_t => NOR1_sck_t,
      NOR1_ss_i(0) => NOR1_ss_i_0(0),
      NOR1_ss_o(0) => NOR1_ss_o_0(0),
      NOR1_ss_t => NOR1_ss_t,
      NOR2_io0_i => NOR2_io0_i,
      NOR2_io0_o => NOR2_io0_o,
      NOR2_io0_t => NOR2_io0_t,
      NOR2_io1_i => NOR2_io1_i,
      NOR2_io1_o => NOR2_io1_o,
      NOR2_io1_t => NOR2_io1_t,
      NOR2_sck_i => NOR2_sck_i,
      NOR2_sck_o => NOR2_sck_o,
      NOR2_sck_t => NOR2_sck_t,
      NOR2_ss_i(0) => NOR2_ss_i_0(0),
      NOR2_ss_o(0) => NOR2_ss_o_0(0),
      NOR2_ss_t => NOR2_ss_t,
      NOR3_io0_i => NOR3_io0_i,
      NOR3_io0_o => NOR3_io0_o,
      NOR3_io0_t => NOR3_io0_t,
      NOR3_io1_i => NOR3_io1_i,
      NOR3_io1_o => NOR3_io1_o,
      NOR3_io1_t => NOR3_io1_t,
      NOR3_io2_i => NOR3_io2_i,
      NOR3_io2_o => NOR3_io2_o,
      NOR3_io2_t => NOR3_io2_t,
      NOR3_io3_i => NOR3_io3_i,
      NOR3_io3_o => NOR3_io3_o,
      NOR3_io3_t => NOR3_io3_t,
      NOR3_sck_i => NOR3_sck_i,
      NOR3_sck_o => NOR3_sck_o,
      NOR3_sck_t => NOR3_sck_t,
      NOR3_ss_i(0) => NOR3_ss_i_0(0),
      NOR3_ss_o(0) => NOR3_ss_o_0(0),
      NOR3_ss_t => NOR3_ss_t,
      PayloadSlice_PPS_tri_o(0) => PayloadSlice_PPS_tri_o(0),
      PayloadSlice_scl_i => PayloadSlice_scl_i,
      PayloadSlice_scl_o => PayloadSlice_scl_o,
      PayloadSlice_scl_t => PayloadSlice_scl_t,
      PayloadSlice_sda_i => PayloadSlice_sda_i,
      PayloadSlice_sda_o => PayloadSlice_sda_o,
      PayloadSlice_sda_t => PayloadSlice_sda_t,
      ProgrammableLogic_rxd => ProgrammableLogic_rxd,
      ProgrammableLogic_txd => ProgrammableLogic_txd,
      RadioSlice_rxd => RadioSlice_rxd,
      RadioSlice_txd => RadioSlice_txd,
      RemotePort_rxd => RemotePort_rxd,
      RemotePort_txd => RemotePort_txd,
      SYS_RST_tri_o(0) => SYS_RST_tri_o(0),
      SecondaryOBC_EthernetSwitch_rxd => SecondaryOBC_EthernetSwitch_rxd,
      SecondaryOBC_EthernetSwitch_txd => SecondaryOBC_EthernetSwitch_txd,
      Supervisor_FRAM_io0_i => Supervisor_FRAM_io0_i,
      Supervisor_FRAM_io0_o => Supervisor_FRAM_io0_o,
      Supervisor_FRAM_io0_t => Supervisor_FRAM_io0_t,
      Supervisor_FRAM_io1_i => Supervisor_FRAM_io1_i,
      Supervisor_FRAM_io1_o => Supervisor_FRAM_io1_o,
      Supervisor_FRAM_io1_t => Supervisor_FRAM_io1_t,
      Supervisor_FRAM_sck_i => Supervisor_FRAM_sck_i,
      Supervisor_FRAM_sck_o => Supervisor_FRAM_sck_o,
      Supervisor_FRAM_sck_t => Supervisor_FRAM_sck_t,
      Supervisor_FRAM_ss_i(0) => Supervisor_FRAM_ss_i_0(0),
      Supervisor_FRAM_ss_o(0) => Supervisor_FRAM_ss_o_0(0),
      Supervisor_FRAM_ss_t => Supervisor_FRAM_ss_t,
      Supervisor_rxd => Supervisor_rxd,
      Supervisor_txd => Supervisor_txd,
      WP_DIS_tri_o(0) => WP_DIS_tri_o(0),
      uart_rtl_rxd => uart_rtl_rxd,
      uart_rtl_txd => uart_rtl_txd
    );
Supervisor_FRAM_io0_iobuf: component IOBUF
     port map (
      I => Supervisor_FRAM_io0_o,
      IO => Supervisor_FRAM_io0_io,
      O => Supervisor_FRAM_io0_i,
      T => Supervisor_FRAM_io0_t
    );
Supervisor_FRAM_io1_iobuf: component IOBUF
     port map (
      I => Supervisor_FRAM_io1_o,
      IO => Supervisor_FRAM_io1_io,
      O => Supervisor_FRAM_io1_i,
      T => Supervisor_FRAM_io1_t
    );
Supervisor_FRAM_sck_iobuf: component IOBUF
     port map (
      I => Supervisor_FRAM_sck_o,
      IO => Supervisor_FRAM_sck_io,
      O => Supervisor_FRAM_sck_i,
      T => Supervisor_FRAM_sck_t
    );
Supervisor_FRAM_ss_iobuf_0: component IOBUF
     port map (
      I => Supervisor_FRAM_ss_o_0(0),
      IO => Supervisor_FRAM_ss_io(0),
      O => Supervisor_FRAM_ss_i_0(0),
      T => Supervisor_FRAM_ss_t
    );
end STRUCTURE;
